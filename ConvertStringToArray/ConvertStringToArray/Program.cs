﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertStringToArray
{
    class Program
    {
        static void Main(string[] args)
        {
            var colours = "red, blue, orange, white, black";
            var rayray = colours.Split(new[] { ", " }, StringSplitOptions.None);
            foreach (var word in rayray)
            {
                Console.WriteLine(word);
            }
        }
    }
}
